#import "@preview/cetz:0.2.2": canvas, draw

#figure(caption: [Simlified NFA for parsing digits], canvas(length: 3cm, {
	import draw: *

	set-style(radius: 0.15, mark: (end: ">", scale: 1.5, fill: black))
	circle((0, 2), name: "q0")
	content((), $q_0$)
	line((rel: (-.5, 0)), "q0.west")
	circle((1, 1), name: "d0", stroke: 2pt)
	content((), $d_0$)
	line("q0.south-east", "d0.north-west")
	circle((1, 0), name: "f", stroke: 2pt)
	content((), $f$)
	circle((0, 0), name: "f0")
	content((), $f_0$)
	line("d0.south-west", "f0.north-east")
	line("f0.east", "f.west")
	arc-through("f.south-east", (rel: (0, -0.25), to: "f.south"), "f.south-west", name: "f-self")
	circle((0, 1), name: "d", stroke: 2pt)
	content((), $d$)
	line("q0.south", "d.north")
	line("d0.west", "d.east")
	line("d.south", "f0.north")
	circle((2, 2), name: "b0")
	content((), $b_0$)
	line("d0.east", (horizontal: ("d0.east", 0.5, "b0.west"), vertical: ()), (horizontal: (), vertical: "b0.west"), "b0.west", name: "d0-b0")
	circle((3, 2), name: "b", stroke: 2pt)
	content((), $b$)
	line("b0.east", "b.west")
	arc-through("b.north-east", (rel: (0.25, 0), to: "b.east"), "b.south-east", name: "b-self")
	circle((2, 1), name: "h0")
	content((), $h_0$)
	line("d0.east", "h0.west")
	circle((3, 1), name: "h", stroke: 2pt)
	content((), $h$)
	line("h0.east", "h.west")
	arc-through("h.north-east", (rel: (0.25, 0), to: "h.east"), "h.south-east", name: "h-self")
	circle((2, 0), name: "o0")
	content((), $o_0$)
	line("d0.east", (horizontal: ("d0.east", 0.5, "o0.west"), vertical: ()), (horizontal: (), vertical: "o0.west"), "o0.west", name: "d0-o0")
	circle((3, 0), name: "o", stroke: 2pt)
	content((), $o$)
	line("o0.east", "o.west")
	arc-through("o.north-east", (rel: (0.25, 0), to: "o.east"), "o.south-east", name: "o-self")

	for i in (
		("q0", "d0", "0"),
		("q0", "d", "1..9"),
		("d0", "d", "0..9"),
		("d0", "f0", "."),
		("d", "f0", "."),
		("f0", "f", "0..9"),
		("f-self", "0..9"),
		("d0-b0", "b"),
		("b0", "b", "0..1"),
		("b-self", "0..1"),
		("d0", "h0", "x", 0.6),
		("h0", "h", "0..f"),
		("h-self", "0..f"),
		("d0-o0", "o"),
		("o0", "o", "0..7"),
		("o-self", "0..7")
	) {
		let p = 0.5
		if i.len() == 4 { p = i.pop() }
		if i.len() == 3 {
			let (l, r, c) = i
			content((l, p, r), box(fill: luma(220), outset: 3pt, radius: 2pt, raw(c)))
		} else {
			let (p, c) = i
			content(p + ".mid", box(fill: luma(220), outset: 3pt, radius: 2pt, raw(c)))
		}
	}
})) <lex-nfa_gen-fig1>
