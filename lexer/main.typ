#import "../prelude.typ": *
#import "@preview/minitoc:0.1.0": minitoc

= Lexing

#minitoc(target: heading.where(level: 2))

#for i in (
    "0-intro.typ", "1-nfa_gen.typ", "2-compress.typ", "3-ser_de_spec.typ", "4-lexer.typ"
) { include i }
