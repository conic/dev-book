#import "../prelude.typ": *

== Introduction

This section explains how lexing is handled by the Conic compiler. This is a more involved process than for most, and requires a significant amount when compared to much simpler lexers, such as regex-based lexers.

The lexer is very similar to an NFA or nondeterministic finite automata. As such, a good portion of this chapter will be much easier to understand with some knowledge of formal language theory.

This section will cover the following key points:
- Generating an NFA from a given language file (@lex-nfa_gen)
- Storing and compressing an NFA (@lex-store)
- The actual process the lexer goes through when lexing an input

This section has one associated appendix: @apdx-tok.
