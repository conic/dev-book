# Conic Development Book

[**Link to book**](https://gitlab.com/conic/dev-book/-/raw/master/Conic%20Development%20Book.pdf)

This repo contains the development book for Conic. This includes language design, some specifications, and internal design and structure.

This is intended to be used/referenced by those developing Conic or anyone interested in how it works.

## Building the PDF

```sh
typst compile main.typ Conic\ Development\ Book.pdf
```
