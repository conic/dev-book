%YAML 1.2
---
name: Conic
file_extensions: [cn, conic]
scope: source.cn

variables:
  identifier: '[[:alpha:]][_[:alnum:]]*|_[_[:alnum:]]+'
  self_idents: '\b[Ss]elf\b'
  escaped_char: '\\([nrt0\"''\\]|x[0-7]\h|u\{(?:\h_*){1,6}\})'

contexts:
  main:
    - include: statements

  visibility:
    - match: \b(pub|pri)\b
      scope: storage.modifier.cn

  statements:
    - match: ^\@({{identifier}})
      scope: keyword.control
    - include: comments
    - include: decorators
    - include: symbols
    - match: '(?=(::\{))'
      scope: storage.type.cn
      push:
        - include: keywords
        - match: '\}'
          pop: true
    - match: '\('
      scope: punctuation.section.group.begin.cn
      push:
        - match: '\)'
          scope: punctuation.section.group.end.cn
          pop: true
        - include: statements
    - include: module
    - include: digit
    - include: literals
    - include: visibility
    - include: function

    - match: '\b(const)\s+({{identifier}})\s+({{identifier}})'
      captures:
        1: storage.type.cn
        2: entity.name.type.cn
        3: variable.other.cn

    - match: \bextension\b
      scope: storage.type.extension.cn
      push: extension

    - match: \bextend\b
      scope: storage.type.extension.cn
      push: extension-impl

    - match: \brepeat\b
      scope: keyword.control.cn
      push: repeat

    - match: \bmatch\b
      scope: keyword.control.cn
      push: match

    - match: \bfor\b
      scope: keyword.control.cn
      push: for

    - match: \bstruct\b
      scope: storage.type.struct.cn
      push: struct-identifier

    - match: \benum\b
      scope: storage.type.enum.cn
      push: enum-identifier
    - include: keywords
      push: struct-value
    - match: '{{self_idents}}'
      scope: storage.type.cn
    - match: '({{identifier}})\s*(\()'
      scope: meta.function-call.cn
      captures:
        1: variable.function.cn
        2: meta.group.cn punctuation.section.group.begin.cn
      push:
        - include: comments
        - match: '\)'
          scope: meta.group.cn punctuation.section.group.end.cn
          pop: true
        - match: ','
          scope: punctuation.separator.cn
        - include: statements
    - match: '{{identifier}}'
    - match: '\.'
      scope: punctuation.separator.cn

    - include: struct-value

  keywords:
    - match: \bset\b
      scope: storage.type.cn
    - match: \buse|to\b
      scope: keyword.other.cn
    - match: \bas|in\b
      scope: keyword.operator.cn
    - match: '\b(return|break|continue)\b'
      scope: keyword.control.cn

  type:
    - match: '(?=($\n?))'
      pop: true
    - match: '\['
      scope: punctuation.section.group.begin.cn
      push:
        - match: '\]'
          scope: punctuation.section.group.end.cn
          pop: true
        - include: type
    - match: 'Self'
      scope: storage.type.cn
      push:
        - match: '(?=<)'
          push:
            generic-angles
        - match: '.'
          pop: true
    - match: '{{identifier}}'
      scope: entity.name.type.cn
      push:
        - match: '(?=<)'
          push:
            generic-angles
        - match: '(?=($\n?))'
          pop: true
        - match: '.'
          pop: true
    - match: '::'
      scope: punctuation.accessor.cn
    - match: '\*'
      scope: keyword.operator.cn
    - match: '(?=\S)'
      pop: true

  module:
    - match: \bmod\b
      scope: storage.type.module.cn
      push:
        - match: '{{identifier}}'
          scope: entity.name.namespace.cn
        - match: '\}'
          scope: meta.block.cn punctuation.section.block.end.cn
          pop: 2
        - match: '\{'
          scope: punctuation.section.block.begin.cn
          push:
            - meta_scope: meta.block.cn
            - match: '(?=\})'
              pop: true
            - include: statements


  struct-value:
    - include: path
    - meta_scope: meta.struct.cn
    - match: '\}'
      scope: meta.block.cn punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push: struct-value-inner
    - match: '(?=\S)'
      pop: true

  struct-value-inner:
    - meta_scope: meta.block.cn
    - include: comments
    - include: visibility
    - include: function
    - match: '\b({{identifier}})\s*(:)'
      captures:
        1: variable.parameter.cn
        2: punctuation.separator.cn
      push:
        - include: statements
        - match: ','
          pop: true
        - match: '(?=\})'
          pop: 2
    - match: '(?=\})'
      pop: true

  pattern-param:
    # A pattern used in a function or closure parameter.
    - include: comments
    - match: '\*'
      scope: keyword.operator.cn
    - match: '\bself\b'
      scope: storage.type.cn
    - match: '\b({{identifier}})\s+({{identifier}})'
      captures:
        1: entity.name.type.cn
        2: variable.other.cn

  comments:
    - include: block-comments
    - match: "\\\\\\\\[!\\\\]"
      scope: punctuation.definition.comment.cn
      push:
        - meta_scope: comment.line.documentation.cn
        - match: '(@param)\s*\('
          captures:
            1: comment.line.documentation.decorator.cn
          push:
            - match: '{{identifier}}'
            - match: '\)'
              pop: true
        - match: $\n?
          pop: true
    - match: "\\\\\\\\"
      scope: punctuation.definition.comment.cn
      push:
        - meta_scope: comment.line.double-slash.cn
        - match: $\n?
          pop: true

  block-comments:
    - match: '\\\\\*[!\*][^\*\\\\]'
      scope: punctuation.definition.comment.cn
      push:
        - meta_scope: comment.block.documentation.cn
        - match: \*\\\\
          scope: punctuation.definition.comment.cn
          pop: true
        # Javadoc style comment with leading * on each line. Helps with word-wrapping.
        - match: ^\s*(\*)(?!\\\\)
          captures:
            1: punctuation.definition.comment.cn
        - include: block-comments
    - match: \\\\\*
      scope: punctuation.definition.comment.cn
      push:
        - meta_scope: comment.block.cn
        - match: \*\\\\
          scope: punctuation.definition.comment.cn
          pop: true
        - include: block-comments

  match:
    - include: comments
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push:
        - include: comments
        - match: '=>'
          scope: keyword.operator.rust
          push:
            - match: '$\n?'
              pop: true
            - include: statements
        - match: '(?=\})'
          pop: true
        - include: literals
    - match: '\}'
      scope: punctuation.section.block.end.cn
      pop: true

  function:
    - match: '\b(fn)\s+({{identifier}})'
      captures:
        1: storage.type.function.cn
        2: entity.name.function.cn
      push: fn-parameters

  fn-parameters:
    - meta_scope: meta.function.cn
    - match: '\)'
      scope: meta.function.parameters.cn punctuation.section.parameters.end.cn
      set: fn-return
    - match: '\('
      scope: punctuation.section.parameters.begin.cn
      push:
        - meta_scope: meta.function.parameters.cn
        - include: comments
        - match: '(?=\))'
          pop: true
        - include: pattern-param

  fn-return:
    - meta_scope: meta.function.cn
    - include: comments
    - match: '(?=\{)'
      set: fn-body
    - match: '->'
      scope: punctuation.separator.cn
      push:
        - include: type
        - match: '(?=\S)'
          pop: true
    # Escape for incomplete expression, or ';'
    - match: '(?=\S)'
      pop: true

  fn-body:
    - include: comments
    - meta_scope: meta.function.cn
    - match: '\}'
      scope: meta.block.cn punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push:
        - meta_scope: meta.block.cn
        - match: '(?=\})'
          pop: true
        - include: statements

  struct-identifier:
    - meta_scope: meta.struct.cn
    - include: comments
    - match: '{{identifier}}'
      scope: entity.name.struct.cn
      set:
        - meta_scope: meta.struct.cn meta.generic.cn
        # - match: '(?=<)'
        #   push: generic-angles
        - match: '(?=\{)'
          push: struct-classic
        - match: '\}'
          pop: true
        - match: '(?=\S)'
          pop: true

  struct-classic:
    - meta_scope: meta.struct.cn
    - match: '\}'
      scope: meta.block.cn punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push: struct-classic-body
    - match: '(?=\S)'
      # Abort for an invalid match.
      pop: true

  struct-classic-body:
    - meta_scope: meta.block.cn
    - include: comments
    - include: visibility
    - include: function
    - match: \bproperties\b
      scope: keyword.other.cn meta.struct.properties.cn
      push: struct-classic-properties
    - match: '(?=\})'
      pop: true
    - match: '(?=\S)'
      # Abort for an invalid match.
      pop: true

  struct-classic-properties:
    - meta_scope: meta.struct.properties.cn
    - match: '\}'
      scope: meta.block.cn punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push: struct-classic-body-properties

  struct-classic-body-properties:
    - include: visibility
    - match: '({{identifier}})\s+({{identifier}})'
      captures:
        1: entity.name.type.cn
        2: variable.other.member.cn
    - meta_scope: meta.struct.properties.cn
    - match: '(?=\})'
      pop: true
    - match: '(?=\S)'
      pop: true

  enum-identifier:
    - meta_scope: meta.struct.cn
    - include: comments
    - match: '{{identifier}}'
      scope: entity.name.struct.cn
      set:
        - meta_scope: meta.struct.cn meta.generic.cn
        # - match: '(?=<)'
        #   push: generic-angles
        - match: '(?=\{)'
          push: enum-classic
        - match: '\}'
          pop: true
        - match: '(?=\S)'
          pop: true

  enum-classic:
    - meta_scope: meta.struct.cn
    - match: '\}'
      scope: meta.block.cn punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push: enum-classic-body
    - match: '(?=\S)'
      # Abort for an invalid match.
      pop: true

  enum-classic-body:
    - meta_scope: meta.block.cn
    - include: comments
    - include: visibility
    - include: function
    - match: \bvariants\b
      scope: keyword.other.cn meta.struct.properties.cn
      push: enum-classic-variants
    - match: '(?=\})'
      pop: true
    - match: '(?=\S)'
      # Abort for an invalid match.
      pop: true

  enum-classic-variants:
    - include: comments
    - meta_scope: meta.struct.properties.cn
    - match: '\}'
      scope: meta.block.cn punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push: enum-classic-body-properties

  enum-classic-body-properties:
    - include: comments
    - meta_scope: meta.enum.variants.cn
    - match: '({{identifier}})\s*(?=\{)'
      captures:
        1: storage.type.source.cn
      push:
        - meta_scope: meta.enum.variants.cn
        - match: '\}'
          scope: meta.block.cn punctuation.section.block.end.cn
          pop: true
        - match: '\{'
          scope: punctuation.section.block.begin.cn
          push: enum-classic-body-property-struct
    - match: '({{identifier}})\s*(?=\()'
      captures:
        1: storage.type.source.cn
      push:
        - meta_scope: meta.enum.variants.cn
        - match: '\)'
          scope: meta.block.cn punctuation.section.block.end.cn
          pop: true
        - match: '\('
          scope: punctuation.section.block.begin.cn
          push: enum-classic-body-property-tuple
    - match: "{{identifier}}"
      scope: storage.type.source.cn
    - match: '(?=\})'
      pop: true
    - match: '(?=\S)'
      pop: true

  enum-classic-body-property-struct:
    - include: comments
    - match: '({{identifier}})\s+({{identifier}})'
      captures:
        1: entity.name.type.cn
        2: variable.other.member.cn
    - meta_scope: meta.struct.properties.cn
    - match: '(?=\})'
      pop: true
    - match: '(?=\S)'
      pop: true

  enum-classic-body-property-tuple:
    - include: types
    - match: ','
    - meta_scope: meta.struct.properties.cn
    - match: '(?=\))'
      pop: true
    - match: '(?=\S)'
      pop: true

  generic-angles:
    - meta_scope: meta.generic.cn
    - match: '>'
      scope: punctuation.definition.generic.end.cn
      pop: true
    - match: '<'
      scope: punctuation.definition.generic.begin.cn
      push: generic-angles-contents
    - match: '(?=\S)'
      pop: true

  generic-angles-contents:
    - include: comments
    - match: '(?=>)'
      pop: true
    - match: '<'
      scope: punctuation.definition.generic.begin.cn
      push:
        - match: '>'
          scope: punctuation.definition.generic.end.cn
          pop: true
        - include: generic-angles-contents
    - match: '{{identifier}}'
      scope: storage.type.cn
    - match: ','
      scope: punctuation.separator.cn
    - match: '\+'
      scope: keyword.operator.cn
    - match: '(?=\S)'
      pop: true

  repeat:
    - include: comments
    - match: '{{identifier}}'
      scope: variable.other.cn
      push:
        - match: '(?=\})'
          scope: punctuation.section.block.end.cn
          pop: true
        - match: '\{'
          scope: punctuation.section.block.begin.cn
          push:
            - match: '(?=\})'
              pop: true
            - include: statements
    - match: '\}'
      scope: punctuation.section.block.end.cn
      pop: true

  for:
    - include: comments
    - match: '\}'
      scope: punctuation.section.block.end.cn
      pop: true
    - match: '({{identifier}})\s+(in)'
      captures:
        1: variable.other.cn
        2: keyword.operator.cn
      push:
        - match: '(?=\})'
          pop: true
        - match: '\{'
          scope: punctuation.section.block.begin.cn
          push:
            - match: '(?=\})'
              pop: true
            - include: statements
        - include: statements

  extension:
    - include: comments
    - match: '\}'
      scope: punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push:
        - include: comments
        - match: '(?=\})'
          pop: true
        - include: extension_inner
    - include: type
    - match: '{{identifier}}'
      scope: variable.other.cn

  extension_inner:
    - include: comments
    - match: '\b(const)\s+({{identifier}})\s+({{identifier}})'
      captures:
        1: storage.type.cn
        2: entity.name.type.cn
        3: variable.other.cn
    - match: '\b(fn)\s+({{identifier}})'
      captures:
        1: storage.type.function.cn
        2: entity.name.function.cn
      push:
        - meta_scope: meta.function.cn
        - match: '\)'
          scope: meta.function.parameters.cn punctuation.section.parameters.end.cn
          set: extension-fn-return
        - match: '\('
          scope: punctuation.section.parameters.begin.cn
          push:
            - meta_scope: meta.function.parameters.cn
            - include: comments
            - match: '(?=\))'
              pop: true
            - include: pattern-param

  extension-fn-return:
    - meta_scope: meta.function.cn
    - include: comments
    - match: '(?=\{)'
      set: fn-body
    - match: '->\s*'
      scope: punctuation.separator.cn
      push:
        - include: type
        - match: '(?=\S)'
          pop: true
    - match: '(?=\S)'
      pop: true

  extension-impl:
    - include: comments
    - match: \bwith\b
      scope: keyword.operator.cn
    - match: '\}'
      scope: punctuation.section.block.end.cn
      pop: true
    - match: '\{'
      scope: punctuation.section.block.begin.cn
      push:
        - include: comments
        - match: '(?=\})'
          pop: true
        - include: statements
    - include: type

  digit:
    - match: '0x[0-9a-fA-F_]+'
      scope: constant.numeric.integer.hexidecimal.cn
    - match: '0b[0-1_]+'
      scope: constant.numeric.integer.binary.cn
    - match: '0o[0-7_]+'
      scope: constant.numeric.integer.octal.cn
    - match: '[0-9_]+'
      scope: constant.numeric.integer.cn

  literals:
    - include: string
    - include: digit
    - match: '\b(true|false)\b'
      scope: constant.language.booleal.cn
    - match: '\['
      scope: punctuation.section.group.begin.cn
      push:
        - match: '\]'
          scope: punctuation.section.group.end.cn
          pop: true
        - match: ','
        - include: statements

  symbols:
    - match: '='
      scope: keyword.operator.assignment.cn

    - match: '[<>]'
      scope: keyword.operator.comparison.rust

    - match: '[-+%/*]'
      scope: keyword.operator.arithmetic.rust
  
  escaped-char:
    - match: '{{escaped_char}}'
      scope: constant.character.escape.rust
    - match: '\\u\{[^}]*\}'
      scope: invalid.illegal.character.escape.rust
    - match: '\\.'
      scope: invalid.illegal.character.escape.rust

  string:
    - match: '"'
      scope: punctuation.definition.string.begin.cn
      push:
        - meta_include_prototype: false
        - meta_scope: string.quoted.double.cn
        - match: '"'
          scope: punctuation.definition.string.end.cn
          pop: true
        - include: escaped-char
        - match: '(\\)$'
          scope: punctuation.separator.continuation.line.cn
