#show: body => {
    set text(font: "Avenir")
    set raw(lang: "conic")
	set raw(syntaxes: "conic-syntax.sublime-syntax", theme: "code-theme.tmTheme")
	show heading.where(level: 1): set text(size: 15pt)
	show raw.where(block: true, lang: "conic"): it => text(fill: rgb("#a2aabc"), it)
	set page(paper: "a4", margin: 1cm)
	body
}

#import "@preview/codly:0.2.0": *
#show: codly-init.with()
#show raw.where(block: true): set text(fill: white)
#show figure.where(kind: raw): set block(breakable: true)
#codly(
  enable-numbers: true, stroke-width: 0pt, radius: 2pt, breakable: true,
	zebra-color: rgb("#1d2433"), fill: rgb("#1d2433"), default-color: white,
	languages: (
	  conic: (name: text(fill: black)[Conic], icon: none, color: rgb("#2ad475")),
	  cn: (name: text(fill: black)[Conic], icon: none, color: rgb("#2ad475"))
  )
)

= Function Disambiguation

```
type Point<const int N> = [bfloat; N]

extend Point<N> {
    fn add(self, Self other) -> Self { return self + other }
}

extend Point<N> with Add<Self> {
    @priority
    fn add(*self, Self rhs) {
        \\ we can use `zip` because `Self` is a static array
        \\ so we have access to all functions `[T; N]` has
        self = self.zip(other).map(|l, r| return l + r)
    }
}

extend Point<N> with Add<T> where T: Into<Self> {
    fn add(*self, T other) {
        self = self + other.into()
    }
}

extend [int; N] with Into<Point<N>> {
    fn into(self) -> Point {
        self.map(Into.into)
    }
}

fn main() {
    \\ have to specify the type
    \\ otherwise it'd have the type `[int; 3]` or `[int]`
    \\ note that the `0` gets coerced into a bfloat (2ary type)
    set Point origin = [0; 3]
    set Point offset = [1, 2, 3]
    \\ call the first function
    set moved = origin.<Point.add>(offset.clone())
    \\ call the second function
    \\ doesn't use function at line 17 because function at
    \\ line 9 is marked with `@priority` so we choose that
    \\ function over the other (note `Self: Into<Self>`)
    origin.<Add.add>(offset)

    \\ if we wanted to use function at ln 17
    \\ we specify the extension conditions
    origin.<Add<:Into>.add(offset)
}
```

#pagebreak()

= Struct

```
\\ public struct. visible everywhere, even externally (if exported)
pub struct MyStruct {
	\\ define the properties of the struct
	\\ all variables with no 'pri' before them are publicly accessible,
	\\ with 'pub' properties externally visible
	properties {
		pub int a
		pri dint a_dyn
		str name
	}

	\\ public constructor. visible everywhere, even externally
	pub fn new(int a, str name) -> Self {
		set a_dyn = a as dint
		return Self { a: a, a_dyn: a_dyn, name: name }
	}

	\\ exposed function. visible only within the package
	fn sqrt(*self) -> Option<int> {
		return self.try_sqrt().ok()
	}

	\\ private function; not public. only to be used by the struct 'internally'
	pri fn try_sqrt(*self) -> Result<int, null> {
		return self.a.try_sqrt().into()
	}
}

\\ Extend MyStruct with Into<int> allowing you to use
\\ `MyStruct as int`
extend *MyStruct with Into<int> {
	fn into(self) -> int {
		return self.a
	}
}
```

= Enum

```
enum MyEnum {
	variants { }
}
```

= Modules

```

pri mod constants {
	const float PI = 3.14159_26535
}

use constants::PI

fn main() {
	set fib_10 = fibonacci(10)
	for fib in fib_10.iter() { print(fib) }
}

\\\ Fibonacci function
\\\ Returns the first `reps` fibonacci numbers starting [0, 1]
\\\ @param(reps) Number of fibonacci numbers to return
fn fibonacci(uint reps) -> [int] {
	match reps {
		0 => return []
		1 => return [0]
		2 => return [0, 1]
	}
	set out = [0, 1]
	set low = 0
	set high = 1
	reps = (reps - 2).max(0)
	repeat reps {
		set new = low + high
		out.push(new)
		low = high
		high = new
	}
	return out
}

extension MyExtension<T> {
	fn into(*self) -> T
}
```
#pagebreak()
```
fn my_fn(float b) -> float {
    return b + 5
}

\\ should this be an int or float?
\\ - int   : the literal is an int
\\ - float : it's used as a float in the code
\\         : also the literal can be coerced into a float easily
set a = 5
a = my_fn(a)
```
