= Git and IDEs

This chapter is going to be quite a bit shorter than the others but nonetheless just as important. It concerns how we would ideally use git and provide a (currently) hypothetical IDE for Conic.

== Problem statement

Consider @git-s1 below:

#figure(caption: [Snippet of some sample code for git examples])[```conic
!!en
set a
/*
    some logic to initialize 'a'
*/
```]<git-s1>

Now, imagine we're a German developer and we want alter how `a` is initialized. To do this, we might end up with something similar to @git-s2.

#figure(caption: [First insertion of German code into @git-s1])[```conic
!!en
set a
{!!de
// some logic goes here
}
```]<git-s2>

This would implement some logic for `a`, but leaves us with a block specifically for German which is perfectly fine but can result in difficult to read code for non-German speakers.
