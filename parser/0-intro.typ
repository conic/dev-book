#import "@preview/cetz:0.2.0": canvas, draw, tree

== Parsing

The parser turns token streams into ASTs (abstract syntax trees). This is done using using the #link("https://gitlab.com/conic/cflp")[`cflp` crate] that was written specifically for this project. This allows the use of attribute proc-macros to generate code to recognise a context-free language. When this is not feasible, custom code can be written for parsing inside a trait seen in @cflp-parser-trait.

#figure(caption: [Parser trait from the `cflp` crate])[```rust
pub trait Parser<I, C: PartialEq<C>, B, R = Self>
    where Self: Scope<B>
{
	fn parse<T: Iterator<Item=I> + Clone>(src: &mut Peekable<T>)
        -> Result<R, Error<I, C, B>>
        where Self: Sized
    {
        <Self as Parser<I, C, B, R>>::parse_with_recursion(src, true)
    }

    fn parse_with_recursion<T: Iterator<Item=I> + Clone>(
	    src: &mut Peekable<T>, recurse: bool
    ) -> Result<R, Error<I, C, B>> where Self: Sized;
}
```]<cflp-parser-trait>

This is implemented for several structs and enuns in the #raw(lang: none, "compiler/lexer") module of the project.

=== Reordering ASTs

Consider the expression `1*2+3`. This would be parsed as the the AST given in @wrong-ast.

#columns(2)[
#figure(canvas(length: 1cm, {
    import draw: set-style
    set-style(content: (padding: .1em))
    tree.tree(([`*`], [`1`], ([`+`], [`2`], [`3`])))
}), kind: "fig", supplement: "Figure", caption: [Generated pre-formatted AST])<wrong-ast>
#colbreak()
#figure(canvas(length: 1cm, {
    import draw: set-style
    set-style(content: (padding: .1em))
    tree.tree(([`+`], ([`*`], [`1`], [`2`]), [`3`]))
}), kind: "fig", supplement: "Figure", caption: [Corrected AST])<fixed-ast>
]

We want our AST to be able to be correctly read bottom up, which @wrong-ast cannot be. However, we can read @fixed-ast bottom up correctly#footnote["Correctly" means obeying mathematical order of operations in this case. This is the only case in which we need to reorder ASTs since we can parse everything else in such a way that the AST is correct immediately after parsing.].

To reformat an AST, we start with any expression that could potentially need reordering. We then traverse the AST with depth-first search until we have a list of all the constituent parts of the expression. For example, the constituent parts of `1*2+(3 * 4)` would be `1`, `2`, and `(3 * 4)` since `(3 * 4)` is in brackets. We then reform the AST by swapping nodes and modifying parent nodes until the AST can be correctly read from the bottom up.
