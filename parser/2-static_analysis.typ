#show "eval ": "evaluation "

== Static evaluation

Static evaluation is the process of taking some code that can be easily executed before compilation and only using the result of the computation instead of the computation itself. For example, we might have `set int a = 1 + 2`. Instead of the compiled code including the addition of 1+2, we just use 3 to save the executable having to perform such a trivial calculation.

Static eval can logically be extended to simulate significant chunks of the project with only read/write, IO, and network requests preventing full simulation. This would, however, make the language interpreted not compiled and would take as long as running the program took. As such, we will require that static eval be fairly quick, providing only small optimisations to the code.

For this section, we'll assume that all operations take the same amount of time to complete. This is fairly standard in computer science, despite it being inaccurate, as it allows much simpler time analysis

=== Arithmetic operations

As previously seen, simple arithmetic operations can be statically evaluated. Since primitive operations cannot be altered by the user, these evaluations will always hold as true.

=== Operation reordering

Some operations can be written in a very inefficient way. For example `(1./2 + 2./2) * 2` could be evaluated to `(0.5 + 1.0) * 2` then to `1.5 * 2` then finally to `3.0`; or we could first simplify it to `(1. + 2.)/2 * 2` to give `1. + 2.` then `3.0`. In this case, the path to the simplified result required the same steps as using the more traditional arithmetic evaluation. However, we can also perform this for expressions with lots of variables in to simplify them for when they're used. If we consider `(t / i) + 6 * k * i`, we can't simplify this using arithmetic expression, but we can simplify it using operation reordering into `(t + 6 * k) / i`, which requires one less operation that the unoptimised version
