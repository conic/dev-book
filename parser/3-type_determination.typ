#import "../prelude.typ": note
#import "@preview/tablex:0.0.5": tablex, rowspanx

== Type determination

Conic is a statically typed language. Because of this, we need to know the type of everything. However, Conic does not require type annotations for everything. For example, you can declare a variable `set a = 5` without the type annotation (`set bfloat a = 5` for example). To allow this, the Conic compiler has a section that analyses types to try and determine what type something should be.

#note[This section consider type determination from a theoretical viewpoint with very little actual code at all. The code comes trivially from the theory and can be found in the `compiler/type_checker` module of the compiler.]

For this section, we will consider declaring a variable with a value as two separate operations:
1. Declaring the variable name `set a`
2. Initialising the variable with a value `a = 5`

=== The main concept

Consider some variable $v$ without an explicit type. To determine its type, we use $T_v$ and $t_v$ to save all the types $v$ could be and update this as we look through the code. $T_v$ contains the primary types, and $t_v$ contains the secondary types (see @primary-secondary-types). Upon declaration, $T_v union t_v = nothing$. If $v$ is initialised with a value, we consider the types that value could be and add those to either $T_v$ and $t_v$. We then go through the code and either add/remove types from $T_v$ and $t_v$ until $v$ goes out of scope. At this point, we have a look at $T_v$ and $t_v$ and check if we know the type of $v$ or not. See @Tv-tv-case-table for how we deal with this.

#figure(
kind: table, caption: [Primary and secondary type set sizes and resultant determined type],
tablex(
columns: (..(20mm,)*2, 60%), align: (..(center + horizon,)*2, left + horizon),
[*$|T_v|$*], [*$|t_v|$*], [*Outcome*],
[1], [N/A], [$v$ is the type in $T_v$],
[>1], [N/A], rowspanx(3)[The compiler cannot determine a valid type for $v$ and requires an explicit type],
rowspanx(2)[0], [0], (),
(), [>1], (),
[0], [1], [$v$ is the type in $t_v$. This raises an error with the level warning],
)
)<Tv-tv-case-table>

=== Primary and secondary types <primary-secondary-types>

Most of the time, we'll be modifying $T_v$. However, we can be lenient with types. Consider @secondary-type-determined-code for example.

#figure(caption: [Type determination for a secondary type])[```cn
fn my_fn(float b) -> float {
    return b + 5
}

set a = 5
a = my_fn(a)
```]<secondary-type-determined-code>

In this code, we first see a function `my_fn` (line 1-3) that takes a `float` and returns a `float`. We then declare a new variable `a` (line 5). If we look at the type of `a`, we could assume that it would be `int` or one of it's possible variants. However, it could also be a `float` or `bfloat` but this is less obvious. Because of this, `float` and `bfloat` are _secondary types_ in this case. Therefore we have $T_#raw("a")={#raw("int")}$ and $t_#raw("a")={#raw("float")}$.

If we then move to line 6, we see that `a` must be a `float` to be passed to `my_fn` leaving us with $T_#raw("a")=nothing$ and $t_#raw("a")={#raw("float")}$. This is the end of the program and therefor the point at which `a` goes out of scope so. We see that $T_#raw("a")=nothing$ but $|t_#raw("a")|=1$, therefore `a` has a known type, but it's a secondary type, meaning we emit a warning level error.
