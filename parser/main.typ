#import "../prelude.typ": *
#import "@preview/minitoc:0.1.0": minitoc

= Parsing

#minitoc(target: heading.where(level: 2))

#for (i, j) in (
    "intro", "node_structure", "static_analysis", "type_determination"
).enumerate() { include str(i) + "-" + j + ".typ" }
