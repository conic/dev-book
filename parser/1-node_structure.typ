#import "../prelude.typ": local_outline

#let node-link(..args) = {
    let lowercase = t => t.codepoints().map(str.to-unicode).map(t => if 65 <= t and t <= 90 { t + 32 } else { t }).map(str.from-unicode).join("")
    args = args.pos()
    if args.len() == 1 {
        let name = args.at(0)
        link(label("ast_" + lowercase(name)))[#raw(name)]
    } else {
        let name = args.at(0)
        let ref = args.at(1)
        link(label("ast_" + lowercase(ref)))[#raw(name)]
    }
}

#let head(title, kind, src) = {
    let w = 100%
    stack(dir: ltr, align(horizon, box(width: w, title)), box(width: 100% - w)[#set align(right);#text(fill: luma(50%), kind)\ #raw(lang: none, src)])
}
#let ehead = (a, b) => head(a, [Enum], b)
#let shead = (a, b) => head(a, [Struct], b)

== Node structure

This section provides all the different AST nodes used by the parser and is primarily for reference only.

For struct nodes#footnote["struct" here referring to the internal data type in the source code], a list of fields is given (or a bulleted list for unnamed structs) with the types linking to the reference for that type; note that all fields are public unless stated otherwise. For enum nodes#footnote[See previous footnote but for "enum"] a list of variants is given with the formatting for each being as described for structs.

Each heading has the datatype to the right of the title with the path to the definition below that (relative to `modules::compiler::parser`). For struct type nodes, the struct fields will be given in the following form:

`field_name`: `field_type`\
#h(2em)Description of the field

Enum type nodes will have all their variants given in the following form:

- `tuple_type_variant_name`:
    - `field_1`: Description of the field
- `struct_type_viriant_name`:
    - `field_name`: `field_type`\
        #h(2em)Description of the field

Some enums will be called _storage enums_. These nodes exist to allow one node to contain several different nodes and contain no additional data. For example, #node-link("Stmt") is a storage enum because it parses no tokens itself but simply holds other nodes. These do not have descriptors for their fields because they are self-evident.

Note that some types will include the type `Wrapper<T>`. This is a type alias for `NodeWrapper<T, Position>` with `NodeWrapper<T, P>` being defined in the `cflp` crate.

#local_outline(depth: 1)

#shead([=== File <ast_file>], "file::File")

Parses a single source code file.

*Fields*

`doc`: #node-link("Documentation", "doc_com")\
#h(2em)Documentation comment

`decorators`: #node-link("Vec<Wrapper<Decorator>>", "decorator")\
#h(2em)Decorators for the file itself

`items`: #node-link("Item")\
#h(2em)Items in the file

*Notes*

Doc comments and decorators for a file must be at the start of the file with at least one newline separating them from any #link(label("ast_item"))[items] in the file. For example, @node-file-good-example would have the doc comment and decorator `@d1` applied to the file, whereas without the newline (seen in @node-file-bad-example), they would be applied with `@d2` to the constant.

#figure(caption: [A file with file decorators])[```cn
\\\ This is a bad example
@d1

@d2
const int T = 1
```]<node-file-good-example>

#figure(caption: [A file with no file decorators])[```cn
\\\ This is a bad example
@d1
@d2
const int T = 1
```]<node-file-bad-example>

#ehead([=== Item <ast_item>], "item::mod::Item")

An item is anything that can be prefixed with a visibility. This is a storage enum.

*Variants*

- `Struct`:
    - #node-link("Struct")
- `Enum`:
    - #node-link("Enum")
- `Fn`:
    - #node-link("Function<FnArg>", "fn")
- `Const`:
    - #node-link("Const")

#ehead([=== Statement <ast_stmt>], "stmt::Stmt")

A statement is anything that can be inside a function body. It's essentially an extension of #node-link("Item"). This is a storage enum.

*Variants*

- `InnerStmt`:
    - #node-link("InnerStmt")
- `Item`:
    - #node-link("Item")

=== Struct <ast_struct>

=== Enum <ast_enum>

=== Function <ast_fn>

=== Const <ast_const>

#ehead([=== Inner Statement <ast_innerstmt>], "inner_stmt::mod::InnerStmt")

An inner statement is any expression that can be inside a function body excluding things under #node-link("Item"). It's an extension of #node-link("Stmt") for use inside blocks.

*Variants*

- `InitAssign`:
    - #node-link("InitAssign")
- `ReAssign`:
    - #node-link("ReAssign")
- `Expr`:
    - #node-link("Expr")

#ehead([=== Expr <ast_expr>], "expr::mod::Expr")

An expression is anything that can be inside a function body excluding things included under #node-link("Item") and #node-link("InnerStmt"). For example arithmetic operations, list comprehensions, or `if` statements

#shead([=== Inital Assignment <ast_initassign>], "inner_stmt::assign::InitAssign")

Initial assignment nodes are for assignment to variables where the statement is prefixed by the `set` keyword. These can omit an expression to initialise, but not assign a value to, a variable.

*Fields*

`ty`: #node-link("Option<Wrapper<Type>>", "type")\
#h(2em)Variable type

`id`: #node-link("Wrapper<Ident>", "Ident")\
#h(2em)Variable name

`val`: #node-link("Option<Wrapper<Expr>>", "Expr")\
#h(2em)Optional expression to assign to the variable

#shead([=== Post-initial Assignment <ast_reassign>], "inner_stmt::assign::ReAssign")

Post-initial assignment (re-assignment) is for assignment of expressions to variables that have been initialised. These require an expression.

#shead([=== Type <ast_type>], "types::Type")
#shead([=== Ident <ast_ident>], "ident::Ident")
#ehead([=== Decorator <ast_decorator>], "decorators::Decorator")

=== Documentation <ast_doc_com>

