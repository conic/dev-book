#import "@preview/minitoc:0.1.0": *
#set raw(lang: "conic")

= Language Design

#minitoc(target: heading.where(level: 2))

#for mod in (
    "primitives", "refs", "data-types", "comments", "vis", "extensions", "iteration", "generics", "decorators", "function_calls", "features", "project_structure"
) { include mod + ".typ" }

#set raw(lang: none)
