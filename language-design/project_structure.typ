#import "@preview/tablex:0.0.5": tablex, rowspanx

== Project structure

Conic projects have the following structure:

```text
─ $ROOT
  ├ .conic
  ├ {src}
  ├ {docs?}
  ├ {tests?}
  └ {benches?}
```

The only file here is `.conic`, which is required. The rest are directories. Directories ending in `?` are optional.

#figure(tablex(
columns: (20mm, 25mm, 20mm, 60%), align: (left, center + horizon, center + horizon, left),
[*Path*], [*Required*], [*Name*], [*Description*],
[`.conic`], rowspanx(2)[Yes], [Static], [Project manifest file. See @project-manifest for more info],
[`src`], (), rowspanx(4)[Dynamic], [Source code for the project],
[`docs`], rowspanx(3)[No], (), [Translated documentation for the project],
[`tests`], (), (), [Unit tests],
[`benches`], (), (), [Bench tests],
), kind: table, caption: [Conic project structure])<project-files-tab>

Note that for @project-files-tab, all the dynamic paths can be altered in the manifest file. If not specified, these default to the language provided paths.

=== Manifest file <project-manifest>

The `.conic` file is the project manifest. This contains all the information about the project from authors, name, and version; to features and dependencies.

The first line of the file is a language code. This indicates the language used for the file. This cannot be changed inside the file.

#figure(caption: [Manifest file layout. Not indicative of the manifest file contents])[```
key1:
  key2: example
  key3:
    key4: value1
    key5: |
      lots of text
      over several lines
  key6: value2
```]<sample-manifest-format>

If we consider @sample-manifest-format, we see that the manifest files are ordered by indentation. We can refer to the _path_ of an entry, such as `key1.key3.key4` for `value1`. Note that the firt part of an entry being `|` indicates that the value is a multiline block of text that's indented one level above the level of the key, such as `key1.key3.key5`.

We define individual values (such as `key4: value1`) as an _item_, items with only a key as a _unit item_, and those with values under them (such as `key3` but not `key5`) as a _block_. For a block, the block name is the first item of the block (`key3` is a block name, with the block being itself and all the items under it).

@manifest-file-spec gives the specification for the manifest file format. Note that the syntax is given in English, with `*` being any valid identifier. If the value in the required column is "Publish", this indicates the value is not required for the file to be valid, but is required for the package to be published. Missing entries that are required for publishing will emit an info level error.

#[
#set raw(lang: none)
#show figure: set block(breakable: true)
#figure(caption: [Manifest file format], kind: table, tablex(
columns: (55mm, 25mm, 50%), align: (left + horizon, center + horizon, left), header-rows: 1,
[*Path*], [*Required*], [*Description*],
[`package.name`], [Yes], [Package name. Must be a valid identifier],
[`package.src`], [No], [Source directory. If not provided, defaults to the language default],
[`package.tests`], [No], [Unit test directory. If not provided, defaults to the language default],
[`package.benches`], [No], [Bench test directory. If not provided, defaults to the language default],
[`package.type`], [No], [Specify the library and application entrypoints],
[`package.type.lib`], [No], [Library entrypoint. If not given, looks for the default file name in the source (`package.src`) directory],
[`package.type.app`], [No], [Application entrypoint. If not given, looks for the default file name in the source (`package.src`) directory],
[`package.version`], [Yes], [Package semantic version],
[`package.authors`], [Publish], [Package authors. Entries are either unit items or blocks. For blocks, the block key is the author name],
[`package.authors.*.email`], [No], [Package author email],
[`package.authors.*.github`], [No], [Package author github account],
[`package.authors.*.gitlab`], [No], [Package author gitlab account],
[`package.license`], [Publish], [Package license. This must be a valid license key from SPDX v3.21],
[`package.description`], [Publish], [A short description of the package],
[`package.readme`], [No], [Path to the package README file],
[`package.homepage`], [No], [Package website],
[`package.repo`], [No], [Package repository URL],
[`package.features`], [No], [Package features. Each entry is either an item or unit item. For an item, the value is a description of what the feature is for],
[`dependencies`], [No], [Project dependencies. Each dependency entry key is the name of the dependency followed by an `@` then either the version, `git`, or `path` for versioned, git style, and path style dependencies. The version can be a semi-complete SemVer version (see @semi-semver). Entries can be unit items, units with the value being a note on the usage of the dependency, or blocks (see below)],
[`dependencies.*@path.path`], [No], [Path to the root of a project for a local dependency],
[`dependencies.*@git.git`], [No], [Git URL to a project for a dependency],
[`dependencies.*@git.branch`], [No], [Git branch for a project for a dependency. If not given, default to `main`],
[`dependencies.{target}`], [No], [Target specific dependencies. These are included only when compiling for a target that meets the provided target condition. For info on target conditions, see @features. The entries in this block are the same as for `dependencies` without the `{target}` subpath],
[`dependencies.dev`], [No], [Development dependencies. These are included when not building a release. The entries in this block are the same as for `dependencies` without the `dev` subpath],
[`dependencies.build`], [No], [Build dependencies. These are included when building and not within the built project. The entries in this block are the same as for `dependencies` without the `build` subpath],
))<manifest-file-spec>

#figure(caption: [Sample manifest file])[```conic-package
en
package:
  name: exampleProject
  type:
    app: app.cn
  version: 0.1.0-rc1
  authors:
    nxe
    some other person:
        email: user@name.com
  license: AGPL-3.0
  description: |
    Sample project to show project structure and
    work out syntax
  features:
    optional_feature: This is optional
dependencies:
  reqwest@^1.2:
    features: json
    dev-features: blocking
    usage: make some web requests. blocking for dev
  dep4@path:
    path: path/to/dep
  dep5@>=1.2,<1.5

  dev:
    dev-dep@1.9: some dev dependency
    linux:
        ldlib@git:
            git: https://gitlab.com/conic/linux-ldlib.git
            branch: nightly
```]

==== Semi-complete SemVer version <semi-semver>

A semi-complete SemVer version is any valid prefix of a SemVer version with no trailing dot (`.`). For example, `1.2` and `0.5.3-alpha` are both valid semi-complete SemVer versions, but `1.5.` is not.

Any part of the version omitted is implicitly a wildcard. For example, `1.0` is implicitly `1.0.*`.

/ Caret prefix: A caret prefix (`^`) indicates the last value given is a minimum value and can be increased. For example, `^2.0.3` means we can use `2.0.7` but not `2.0.1`. We could also have `^0.5` which would allow using `0.9.1` but not `0.3`.
/ Wildcard: A wildcard allows the value to be filled with anything. For example, `2.0.*` would allow `2.0.5` but not `2.1.3`. A wildcard on its own is also allowed and will match all versions
/ Ranges: Comparison operators can prefix a semi-complete SemVer version to indicate a specific range of acceptable versions. For example, `>=2,<2.0.6` would allow versions from `2.0.0` to `2.0.5`. Ranges are separated with a comma.
]
