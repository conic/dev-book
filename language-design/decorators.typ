#import "../prelude.typ": local_outline
== Decorators

Decorators are additional bits of code added onto data types or functions that can add new behaviour or add generated code.

=== Standard decorators

#local_outline()

==== Prioritising

```
@prioritise
@priority(<integer>)
```

The `priorise` deorator is used to set the priority of a function. Consider the following:

```
fn some_generic_op<T>(T v1, T v2) -> T where T extends Add<T> {
	/* some code */
}

fn some_generic_op<T>(T v1, T v2) -> T where T extends FastAdd<T> {
	/* some code */
}
```

This is an example of function polymorphism, where two functions have the same name, but different requirements. In this case, we might be writing some code where performance is important, so we have some types that extend `FastAdd` that's a faster version of `Add`, and some only extend `Add` (a real world example might be adding two upper triangular matrices. We could add them elementwise, or just add the non-zero elements taking around half the time). We want to use the second version of the function if we can, but otherwise we want to default to the first version. To indicate this, we add `@prioritise` to the seccond version of the function.

If we had several versions of the same function, we would instead use `@priority` with a priority level; the higher the priority level, the more important the function. In our example above, say we had another function that was somewhere between the first and second version in terms of performance. We would add `@priority(10)` to this to indicate that it's a higher priority that the first version which has a priority level of 0 by default, and the fastest version which has `@prioritse` on it which gives it higher priority than anything else.

If we had this third version, our final code would look a little like this:

```
fn some_generic_op<T>(T v1, T v2) -> T where T extends Add<T> {
	/* some code */
}

// we use 10 instead of 1 to give us room to add versions with
// worse performance below this version
@priority(10)
fn some_generic_op<T>(T v1, T v2) -> T where T extends SemiFastAdd<T> {
	/* some code */
}

@prioritise
fn some_generic_op<T>(T v1, T v2) -> T where T extends FastAdd<T> {
	/* some code */
}
```

==== Since

```
@since(<version>)
```

The `@since` decorator is used to indicate when something was added to a project. This is added into documentation.

The `@since` decorator can be added automatically using the `conic util since add` command.

*Example:*
```
@since(0.1.1)
pub const float PI = 3.14159265358979323846264338327950288

@since(0.1.2)
pub const float E = 2.71828182845904523536028747135266250
```

==== Unstable

```
@unstable
```

The `@unstable` decorator is used to indicate that something is unstable and can have undefined behaviour.

==== Nightly

```
@nightly
```

The `@nightly` decorator is used to indicate that the item is subject to possibly significant change or removal in future versions.

*Example:*
```
@since(0.1.0-rc1)
@nightly
pub fn some_new_feature( /* args */ ) { /* ... */ }
```

==== Feature

```
@feature(<feature conditions>)
```

The `@feature` decorator is used to specify that the item below it requires the given feature condition to be true for it to be included in the code. See @features for more info.

==== Target

```
@target(<target condition>)
```

The `@target` decorator is used the same way the `@feature` decorator is used. See @features for more info.
