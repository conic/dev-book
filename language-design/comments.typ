== Comments

Conic uses C-style comments:
/ Single line: `//`
/ Block: `/* ... */`

```
set a = 1 + 2 // initialize a variable
/*
Add 1 to a
You could also increment a with ++
*/
a += 1
```

=== Doc comments

Doc comments use either the line style or block style comment with an extra backslash at the start:
/ Single line doc: `///`
/ Block: `//* ... */`

Doc comments are treated as git-flavoured markdown with some additions custom to Conic.

These must preface something that can be documented such as a constant or function, otherwise it's entirely ignored and treated as a normal comment. The things that can be documented are:
- Structs
- Enums
- Extensions
- Functions
- Modules and files (see @file_and_mod_doc_comments below)
- Consts
- Variables

==== File and module doc comments <file_and_mod_doc_comments>

To document a file, you place the doc comment at the top of the file and use `//!` instead of `///` or `/!*` instead of `//*`. This indicates that the documentation is for the file and not something in the file.

This can also be used to document modules

==== Generics, arguments, and return types for functions

To document generics and arguments, you use `@generic` and `@arg` followed by the generic or argument identifier then the documentation for it. You can also use `@generic` and `@arg` with an identifier to reference the relevant item.

Return types use `@return` followed by the type returned and documentation for it#footnote[Note that if a function returns a tuple, you need to include the brackets of the tuple inside the brackets of `@return`; e.g. `@return((int, dint))`].

```
/// Sample function
/// @generic(T) The item in the list @arg(src)
/// @arg(src) List to do some things to
/// @return Some value to do with the list @arg(src)
fn example<T>(*List<T> src) -> int { /* ... */ }
```

==== Struct properties

Struct properties are documented above each property

```
struct StructName<T> where T: Add {
    properties {
        /// Some value
        T a
        /// Some other value
        T b
    }

    /// This is documented like a normal function, but you can optionally ignore the 'self' argument
    fn add(*self) -> T { a + b }
}
```

==== Enum variants

Enum variants are documented above each variants, with fields documented using `@arg` for struct-like variants, and `@data` for the data inside a tuple-like variant.

#pagebreak()

```
enum EnumName {
    variants {
        /// Struct-like variant
        /// @arg(field1) some integer
        /// @arg(field2) some float
        Variant1 {
            int field1,
            float field2
        }
        /// Tuple-like variants
        /// @data(0) First value
        /// @data(1) second and third values together
        Variant2(int, [int])
        /// Another tuple-like variant
        /// @data This documents all the data and cannot
        ///     be used with the indexed version (above)
        Variant3(float, [float])
    }
}
```

==== Extensions and consts

Extensions and constants can use `@generic` in documentation. Functions in extensions are documented like normal extensions.
