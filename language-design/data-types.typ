#import "../prelude.typ": note

== Data Types

This section contains the design for data types:
- Structs
- Enums
- Constant
- Type aliases

=== Structs

```conic
struct StructName {
	properties {
		prop_type a
		prob_type b
	}

	fn add(*self) -> prop_type { a + b }
}
```

Structs are defined using the `struct` keyword, optionally prefixed with either `pub` or `pri` to change the visibility.

=== Enums

```conic
enum EnumName {
	variants {
		Variant1 {
			int field1,
			float field2
		}
		Variant2(int, [int])
		Variant3
	}
}
```

Enums are defined using the `enum` keyword, optionally prefixed with either `pub` or `pri` to change the visibility. Enum variants are either tuple-, struct-, or empty-type variants:
/ Tuple-type: `Enum::TupleVariant(field1, field2)`
/ Struct-type: `Enum::StructVariant { field1: value1, field2: value2 }`
/ Empty-type: `Enum::EmptyVariant`

#note[Note that empty-type variants are equivalent to tuple- or struct-type variants with no fields]

=== Constants

```conic
const uint ID = 5
```

Constants are defined using the `const` keyword, followed by the type of the constant, the identifier, and finally the value. Constants can optionally be prefixed with a visibility.

The type of a constant cannot be a generic type.

=== Type aliases

```conic
type alias_name = type_to_be_aliased
```

Type aliases are defined using the `type` keyword and allow one identifier to be used in the place of another. This can be used to simplify complex types into a simpler type or give a commonly used type a shorter or more meaningful name.

Type aliases can be generic, as can the type being aliased. Type aliases can be prefixed with a visibility and suffixed with generic restrictions:

```conic
pri const TupleList<A, B> = List<(A, B)> where A: Add<B>
```
