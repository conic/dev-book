#import "../prelude.typ": note

== Extensions

Extensions are groups of functions that can be applied to any data type. We use the notation (in this book and in code) `A: B` to indicate that data type `A` implements extension `B`; for example, `int: Add<Self>` means that we can add two `int` types together.

```
extend DataType with Extension {
	\* ... *\
}
```

Some useful extensions are:
- ```
  \\\ Convert one value into another using the `as` keyword
  extension Into<T> {
    \\\ Convert the value after cloning it
  	fn into(self) -> T
  }
  ```
  You can pair this with `extend *DataType with Into<T> { /* ... */ }` to prevent implicit cloning before converting one value into another
- ```
  /// Format `self` into a string. Used when printing the value or when putting it in an f-string
  extension Format {
    \\\ Format `self` using the provided formatter to return a string
    \\\ @param(formatter) Requested format to provide
  	fn fmt(*self, Formatter formatter) -> str
  }
  ```

=== What can go in an extension

Extensions can have:
1. function call signatures
2. complete functions
3. constant signatures
4. completed constants

#note[Completed types can be overwritten]

```
extension ExampleExtension {
	\\ Constant signature. Must be filled when extending a type with this extension
	const int EXTENSION_CONST

	\\ Function signature
	fn func1(*self) -> int

	\\ Function with a body. This can be overwritten when extending using this extension, but must keep the same function signature
	fn func2(*self) -> str {
		return <Self as ExampleExtension>::func1(self) as str
	}
```

Anything under an extension that's a signature is public and cannot be prefixed with a visibility. Anything completed is also public unless given the `pri` visibility. Private items are only visible inside the extension and are thus intended to be for internal use. Extensions must have at least one non-private item.

=== The `Self` type

`Self` is a shorthand for whatever type is being considered at that point. If we look at @Self-kwd-example, we see two usages of the keyword `Self`.

#figure(caption: [Usage of the `Self` keyword])[```cn
struct MyStruct {\
    properties { \* ... *\ }

    fn f() -> int { \* ... *\ }
}

extend MyStruct with Into<T> where T: Into<Self>, int: Into<T> {
    fn into(self) -> T {
        return Self.f().into()
    }
}
```]<Self-kwd-example>

Both usages refer to `MyStruct` in this case since we are implementing the extension `Into<T>` _for_ the data type `MyStruct`.

`Self` inside a struct or enum definition refers to the struct or enum.
