== Generics <lang_design-generics>

Generics are one way Conic achieves polymorphism; specifically parameterised polymorphism.

=== Generics inside definitions

When defining a new data type (not a constant), we can specify what generics we want to use:

```
struct SplitList<T> where T: Cmp {
	properties {
		List<T> lower
		List<T> equal
		List<T> upper
		pri T split_point
	}

	fn new(T split_point) -> Self {
		return Self {
			lower: List::new(),
			equal: List::new(),
			upper: List::new(),
			split_point: split_point
		}
	}

	fn push(*self, T value) {
		match T.<Cmp.compare>(*self.split_point, *value) {
			Ordering::Less => self.lower.push(value)
			Ordering::Equal => self.equal.push(value)
			Ordering::Greater => self.upper.push(value)
		}
	}
}
```

This means that for any instance of `SplitList`, the generic `T` must be extended by the `Cmp` extension (this allows values to be compared); the `where T: Cmp` is called the generic constraint. Notice that for `SplitList::new`, the given return type is `Self` with no generics. This is because `Self` is simply an alias for whatever `Self` actually is; in this case it would be an alias for `SplitList<T>`.

The same applies for functions with generic arguments:

```
fn genericFn<F, T>(callable: F, value: T) where F: Callable<(T), T> {
	print(callable(value))
}
```

An important note is that generics are positional. If you were to use `genericFn<A, B>`, `A` would have to be extended by `Callable<(B), B>`. You cannot specify what arguments you want where.

=== Generics everywhere else

When using values that use generics outside of definitions, you don't need to specify the conditions upon them; the compiler will work that out and make sure that the conditions are met. If we use `SplitList` from above, we might have the following:

```
fn extend_split_lists<Generic>(*SplitList<Generic> list1, SplitList<Generic> list2) {
	for val in list2.lower { list1.push(val) }
	for val in list2.equal { list1.push(val) }
	for val in list2.upper { list1.push(val) }
}
```

Here we see that we're using the generic `Generic` (generics are just normal identifiers) with the constraints omitted. This means that whenever you call this function, the constraints on `Generic` will be checked by the compiler to make sure that they're met.

=== Constant generics

Normally, generics are for types that get worked out when you use the type. However, you can also have constant generics. These use part of the syntax for defining constants:

```
struct SquareMatrix<const uint N> {
    properties { inner: [[float; N]; N] }

    fn width(*self) -> uint {
        return N
    }

    fn elements(*self) -> uint {
        return N * N
    }
}
```

You can use the constant just like any other constant as you can see in `SquareMatrix::elements` and `SquareMatrix::width`.
