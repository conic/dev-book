#import "../prelude.typ": note

== Function Disambiguation <s:fn-disambig>

#figure(caption: [Function disambiguation example])[```cn
extension E1 {
    fn inner(*self) { }
}

extension E2 {
    fn inner(*self) { }
}

extend int with E1 { }
extend int with E2 { }

fn main() {
    set int a = 5
    a.inner()
}
```]<fn-disambig>

If we consider @fn-disambig, we can see that we're unable to determine if we wanted to use `inner` from `E1` or `E2`. To fix this, we need to specify which extension we want to use. This can be done by having #raw(lang: "cn", "a.<E1.inner>()") if we wanted to use `inner` from `E1` and #raw(lang: "cn", "a.<E2.inner>()") for `E2`.

However, if one of the functions we need to specify isn't from an extension, we don't have anything to prefix the function name. In this case we would use `Self` instead as seen in @self-fn-disambig.

#figure(caption: [Function disambiguation for a function not defined in an extension])[```cn
extension E1 {
    fn inner(*self) { }
}
extend int with E1 { }

extend int {
    fn inner(*self) { }
}

fn main() {
    set int a = 5
    a.<Self.inner>()
}
```]<self-fn-disambig>

=== Function disambiguation with generics

#figure(caption: [Function disambiguation example])[```cn
struct Example {
    properties { int val }
}

extend Example with Add<Self> {
    @priority
    fn add(*self, Self rhs) {
        self.val += other.val
    }
}

extend Example with Add<T> where T: Into<Self> {
    fn add(*self, T other) {
        self = self + other.into()
    }
}

fn main() {
    set a = Example { val = 1 }
    set b = Example { val = 1 }
    a = a + b
}
```]<fn-generics-disambig1>

Upon an initial inspection of @fn-generics-disambig1, we might assume that there's no function ambiguity. However, we must remember that `Self: Into<Self>` meaning that the code on line 22 could call either extension implementation. To disambiguate this, we can either specify we want to use the first implementation with `a.<Add<Self>.add>(b)`, or the second implementation with `a.<Add<:Into<Example>>.add>(b)`. Notice the colon `:` prefixing `Into` in the second case denoting that `Into` is an extension not a type. For multiple extensions, you prefix the first with a colon and separate all of them with a plus such as `Add<:Into<Example>+Add<Self>>`.

Importantly, `Self` cannot be used in function disambiguation calls.
