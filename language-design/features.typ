#import "@preview/tablex:0.0.5": tablex

== Features and targets <features>

Conic has two decorators to allow for conditional compilation based on _features_ and _targets_.

=== Features

Features are compile-time options set to either `true` if given or `false` otherwise. These allow you to have additional sections of code only if the end-user wants it. Features are user-specified but must be a valid identifier.

If you want to allow aliases for features, you can specify these in the project config file:

```conic-project
package:
    features:
        feature-name: { lang_code: alternative_name }
```

=== Targets

The `@target` decorator requires a target condition. This means that you can have code that's only compiled for specific targets, such as Linux, OSX, or Windows. You can be more specific with target conditions however.

==== Single target conditions

There are two types of target condition:
1. Operating System condition\
    This is prefixed with an `o:` and can either be; `linux`, `osx`, `windows`, or `other`
2. Architecture condition\
    This is prefixed with an `a:` and is the compared to the first part of the target triple for the target. Some valid values are `x86`, `aarch64`, `armv7`, and `i686`.

=== Multiple conditions

You can use boolean operators and brackets to combine multiple conditions. Some examples and a description of them are given below:
- `@feature(f1 || f2)`\
    This will include the item it's on if either feature `f1` or `f2` are enabled
- `@target(o:linux || (o:osx && a:x86))`\
    This will include the item it's on if the code is being compiled for a Linux OS, or MacOS with an x86 architecture


