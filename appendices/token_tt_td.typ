== Token (TT, TD) pairs <apdx-tok-tt_td>

#figure(caption: [TT and TD value pairs for token types], kind: table, supplement: "Table", [
#import "@preview/tablex:0.0.8": tablex, rowspanx, colspanx, cellx, hlinex, vlinex
#set text(size: 9pt)
#box(width: 130%, tablex(
    columns: (auto,) * 14, align: center + horizon,
    hlinex(start: 2, end: 14), vlinex(start: 2, end: 21),
    cellx(colspan: 2, rowspan: 2)[], (), colspanx(12, [TT]),
    (), (), ..range(11).map(t => [#{t + 1}]), [255],
    rowspanx(19, [TD]), [0], [`Bool(true)`], [`Op(Plus)`], [`Cmp(Eq)`], [`LParen`], rowspanx(19, rotate(90deg, reflow: true)[`Set`]), rowspanx(17, rotate(90deg, reflow: true)[`ControlKeyword`]), rowspanx(11, rotate(90deg, reflow: true)[`DataKeyword`]), rowspanx(10, rotate(90deg, reflow: true)[`PrimitiveKeyword`]), [`Vis(Pub)`], rowspanx(19, rotate(90deg, reflow: true)[`Identifier`]), [`NewLine(Implicit)`], rowspanx(19, rotate(90deg, reflow: true)[`Comment`]),
    (), [1], [`Bool(false)`], [`Op(Minus)`], [`Cmp(NE)`], [`RParen`], (), (), (), (), [`Vis(Pri)`], (), [`NewLine(Explicit)`], (),
    (), [2], [`Int` base 10], [`Op(Mod)`], [`Cmp(LT)`], [`LParenSquare`], (), (), (), (), rowspanx(17, fill: gray)[], (), rowspanx(17, fill: gray)[], (),
    (), [3], [`Int` base 2], [`Op(Mult)`], [`Cmp(GT)`], [`RParenSquare`], (), (), (), (), (), (), (), (),
    (), [4], [`Int` base 16], [`Op(Div)`], rowspanx(15, fill: gray)[], rowspanx(15, fill: gray)[], (), (), (), (), (), (), (), (),
    (), [5], [`Int` base 8], [`Op(Pow)`], (), (), (), (), (), (), (), (), (), (),
    (), [6], [`Float`], [`Op(BinAnd)`], (), (), (), (), (), (), (), (), (), (),
    (), [7], rowspanx(12, fill: gray)[], [`Op(BinOr)`], (), (), (), (), (), (), (), (), (), (),
    (), [8], (), [`Increment`], (), (), (), (), (), (), (), (), (), (),
    (), [9], (), [`Decrement`], (), (), (), (), (), (), (), (), (), (),
    (), [10], (), [`Not`], (), (), (), (), (), (), rowspanx(9, fill: gray)[], (), (), (),
    (), [11], (), [`Colon`], (), (), (), (), (), rowspanx(8, fill: gray)[], (), (), (), (),
    (), [12], (), [`QuestionMark`], (), (), (), (), (), (), (), (), (), (),
    (), [13], (), [`Dot`], (), (), (), (), (), (), (), (), (), (),
    (), [14], (), [`Comma`], (), (), (), (), (), (), (), (), (), (),
    (), [15], (), [`At`], (), (), (), (), (), (), (), (), (), (),
    (), [16], (), [`Arrow(Single)`], (), (), (), (), (), (), (), (), (), (),
    (), [17], (), [`Arrow(Double)`], (), (), (), rowspanx(2, fill: gray)[], (), (), (), (), (), (),
    (), [255], (), cellx(fill: gray)[], (), (), (), (), (), (), (), (), (), (),
))])<tok_tt_td_table>

*Notes*
+ Identifiers inside the `Op`, `Arrow`, `Cmp`, `Vis`, and `Newline` types are identifiers for variants of enums of the same respective name
+ `Op(BSL)` and `Op(BSR)` have no (TT, TD) pairs. Since we use angled brackets (specifically less than `<` and greater than and `>`) for function disambiguation (see @s:fn-disambig) we cannot parse `>>` as a bit-shit right operator since it may be two closing angle brackets
+ `Cmp(LTE)` and `Cmp(GTE)` have no (TT, TD) pairs. If we consider the operator `>>=` we have a bit-shift right assignment. But, since we do not parse `Op(BSR)` (see above), this would be parsed as `Cmp(GT)` then `Cmp(GTE)` which is incorrect
+ There is no (TT, TD) pair for `String` or `Char` types despite being variants of `TokType`. These are only ever manually constructed and not handled by code since they require additional processing; that being escaped characters for `String` and ensuring character correctness for `Char`
+ There is no (TT, TD) pair for curly brackets or braces (`{` and `}`) as these require significantly more processing since they facilitate the language scope aspect of Conic
+ All the keyword token types take their specific TD values from indexes in the language file specification, which can be found in @apdx-lang_spec-spec#footnote[The indexes are one more than the keyword TD value. For example, `else` would have a (TT, TD) of `(6, 5)`.]
+ Anything that extends to TD 255 (`Identifier` and `Comment`) will accept any TD value since it's never checked. However, they will realistically never find a TD value other that 0