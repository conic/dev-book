#import "../prelude.typ": *
= Tokens <apdx-tok>
#local_outline()

This appendix contains the token enums used by the compiler as well as the TT,TD pairings used to construct tokens.

== Token enums <apdx-tok-enum>

These are not exact copies; they have all comments and derive macros removed. The specific layout too has been altered to take up less space.

```rust
pub enum TokType {
	Int(BigUint), Float(f64), Bool(bool),
	String(Vec<u8>), Char(char),
	Op(Op), Cmp(Cmp), Set,
	Increment, Decrement,
	LParen, RParen,
	LParenCurly, RParenCurly,
	LParenSquare, RParenSquare,
	Not, Colon, QuestionMark,
	Dot, Comma, At,
	Arrow(Arrow), Vis(Vis),
	Identifier(String, Vec<u8>),
	ControlKeyword(ControlKeyword),
	DataKeyword(DataKeyword),
	PrimitiveKeyword(PrimitiveKeyword),
	NewLine(NewLine),
	Comment(String, Vec<u8>),
}

pub enum Op {
	Plus, Minus,
	Mod, Mult,
	Div, Pow,
	BinAnd, BinOr,
	BinBSL, BinBSR,
	Any
}

pub enum Cmp {
    Eq, NE,
    LT, LTE,
    GT, GTE,
    Any
}

pub enum Vis {
	Pub, Pri
}

pub enum Arrow {
    Single, Double
}

pub enum Newline {
    Implicit, Explicit
}
```

#pagebreak()

#include "token_tt_td.typ"
