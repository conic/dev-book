= List of items

#[
#set outline(title: none)
// == List of Figures
// #outline(target: figure.where(kind: image))

== List of Tables
#outline(target: figure.where(kind: table))

== List of Listings
#outline(target: figure.where(kind: raw))

]
